# k8s RadosGW

Ceph RadosGW image, running s3 Gateways in Kubernetes.

Originally based on https://github.com/ceph/ceph-container/

Build with:

```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```


Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description             | Default                                                          |
| :---------------------------- | -------------------------- | ---------------------------------------------------------------- |
|  `CLUSTER`                    | Ceph Cluster Name          | `ceph`                                                           |
|  `FSID`                       | Ceph Filesystem ID         | undef (not required if `/etc/ceph/$CLUSTER.conf` exists)         |
|  `KEYRING_NAME`               | Ceph RadosGW Keyring Name  | `$RGW_NAME.gateway`                                              |
|  `MON_HOST`                   | Ceph Mon Hosts             | undef (not required if `/etc/ceph/$CLUSTER.conf` exists)         |
|  `MON_INITIAL_MEMBERS`        | Ceph Mon Initial Members   | `mon1` (not required if `/etc/ceph/$CLUSTER.conf` exists)        |
|  `RGW_FRAMEWORK`              | Frontend Framework         | `beast` - used to be `civetweb`                                  |
|  `RGW_NAME`                   | Ceph RadosGW Client Name   | `radosgw`                                                        |
|  `RGW_PORT`                   | Ceph RadosGW Port          | `8080` (not required if `/etc/ceph/$CLUSTER.conf` exists)        |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point                             | Description                      |
| :---------------------------------------------- | :------------------------------- |
|  `/etc/ceph`                                    | Ceph Cluster Configuration       |
|  `/var/lib/ceph/radosgw/$CLUSTER-rgw.$RGW_NAME` | Ceph RadosGW Keyring             |


Deploy RadosGW connecting to Arbitrary Cluster
-----------------------------------------------

Either mount your Keyring as `/var/lib/ceph/radosgw/$CLUSTER-rgw.$RGW_NAME/keyring`
Or as `/etc/ceph/$CLUSTER.client.$KEYRING_NAME.keyring`.

Either mount your Ceph cluster configuration as `/etc/ceph/$CLUSTER.conf`.
Or set the `MON_HOST`, `MON_INITIAL_MEMBERS`, `FSID`, connecting to your cluster.

Assuming the following Ceph configuration:

```sh
# grep -vE '^(#|[ ]*$)' /etc/ceph/ceph.conf
[global]
fsid = f980b615-746a-4e5e-b429-a364fd69838b
mon initial members = mon1,mon2,mon3
mon host = [v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]
...
```

Create a keyring for RadosGW:

```sh
# ceph auth get-or-create client.radosgw.gateway osd 'allow rwx' mon 'allow rwx' -o /etc/ceph/ceph.client.radosgw.gateway.keyring
# cat /etc/ceph/ceph.client.radosgw.gateway.keyring
[client.radosgw.gateway]
        key = abcdefxxx==
```

Deploy RadosGW (if non-FQDN names involved: replace with IPs or FQDNs)

```sh
# oc process -f deploy/openshift/secret.yaml \
    -p CEPH_INITIAL_MEMBERS=mon1,mon2,mon3 \
    -p CEPH_CLUSTER_ID=f980b615-746a-4e5e-b429-a364fd69838b \
    -p CEPH_MON_HOSTS="[v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]" \
    -p RADOSGW_KEYRING_KEY=abcdefxxx== \
    -p RADOSGW_KEYRING_NAME=radosgw.gateway | oc apply -f-
# oc process -f deploy/openshift/run-ephemeral.yaml \
    -p RADOSGW_KEYRING_NAME=radosgw.gateway | oc apply -f-
```

