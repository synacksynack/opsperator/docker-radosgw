#!/bin/bash

set -e
if test "$DEBUG"; then
    set -x
fi

export LC_ALL=C

CLUSTER=${CLUSTER:-ceph}
HOSTNAME=`uname -n | cut -d. -f1`
RGW_FRAMEWORK=${RGW_FRAMEWORK:-beast}
RGW_NAME=${RGW_NAME:-radosgw}
if ! test -s /etc/ceph/$CLUSTER.conf; then
    if test -z "$MON_HOST" -o -z "$FSID"; then
	echo "CRITICAL: MON_HOST and FSID are required"
	exit 1
    elif test -z "$RGW_APIS"; then
	RGW_APIS="admin, s3"
    fi
    MON_INITIAL_MEMBERS=${MON_INITIAL_MEMBERS:-mon1}
    RGW_PORT=${RGW_PORT:-8080}
fi

if test -z "$HOST_FQDN"; then
    HOST_FQDN=`cat /proc/sys/kernel/hostname`
fi
if test "$HOSTNAME" != "$HOST_FQDN"; then
    if test -d "/var/lib/ceph/radosgw/$CLUSTER-$HOST_FQDN"; then
	echo "overriding hostname with $HOST_FQDN"
	HOSTNAME=$HOST_FQDN
    fi
fi

if test -z "$KEYRING_NAME"; then
    KEYRING_NAME=$RGW_NAME.gateway
fi
RGW_KEYRING=/var/lib/ceph/radosgw/$CLUSTER-rgw.$RGW_NAME/keyring

mkdir -p `dirname $RGW_KEYRING` /var/run/ceph

sed "s|^ceph:.*|ceph:x:`id -g`:|" /etc/group >/var/run/ceph/group
sed \
    "s|^ceph:.*|ceph:x:`id -u`:`id -g`:ceph:/var/lib/ceph:/bin/false|" \
    /etc/passwd >/var/run/ceph/passwd

export NSS_WRAPPER_PASSWD=/var/run/ceph/passwd
export NSS_WRAPPER_GROUP=/var/run/ceph/group
if test -s /usr/lib64/libnss_wrapper.so; then
    export LD_PRELOAD=/usr/lib64/libnss_wrapper.so
else
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi

if ! test -s $RGW_KEYRING; then
    if ! test -s /etc/ceph/$CLUSTER.client.$KEYRING_NAME.keyring; then
	echo "CRITICAL: /etc/ceph/$CLUSTER.client.$KEYRING_NAME.keyring does not exist"
	exit 1
    fi
    cat /etc/ceph/$CLUSTER.client.$KEYRING_NAME.keyring >$RGW_KEYRING
fi

if ! test -e /etc/ceph/$CLUSTER.conf; then
    cat <<EOF >/etc/ceph/$CLUSTER.conf
[global]
fsid = $FSID
mon initial members = $MON_INITIAL_MEMBERS
mon host = $MON_HOST
rgw admin entry = admin
rgw enable apis = $RGW_APIS
rgw enable usage log = true
rgw_frontends = $RGW_FRAMEWORK port=$RGW_PORT
EOF
fi

chown -R ceph:ceph /var/run/ceph/ /var/lib/ceph /etc/ceph >/dev/null 2>&1 || true

/usr/bin/radosgw --cluster $CLUSTER \
		 --setuser ceph \
		 --setgroup ceph \
		 -d -n client.$KEYRING_NAME \
		 -k $RGW_KEYRING &
rpid=$!

wait $rpid

exit $?
